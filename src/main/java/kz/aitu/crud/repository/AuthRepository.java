package kz.aitu.crud.repository;

import kz.aitu.crud.model.Auth;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface AuthRepository extends CrudRepository<Auth, Long> {
    @Modifying
    @Transactional
    @Query("update Auth c set c.username =:username where c.id =:ID")
    public void updateUsernameByID(@Param("username") String username, @Param("ID") long id);
}
