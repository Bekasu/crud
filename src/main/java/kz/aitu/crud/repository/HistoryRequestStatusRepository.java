package kz.aitu.crud.repository;

import kz.aitu.crud.model.HistoryRequestStatus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface HistoryRequestStatusRepository extends CrudRepository<HistoryRequestStatus, Long> {
    @Modifying
    @Transactional
    @Query("update HistoryRequestStatus c set c.status =:status where c.id =:ID")
    public void updateStatusByID(@Param("status") String status, @Param("ID") long id);
}
