package kz.aitu.crud.repository;

import kz.aitu.crud.model.TempFiles;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface TempFilesRepository extends CrudRepository<TempFiles, Long> {
    @Modifying
    @Transactional
    @Query("update TempFiles c set c.filebinarybyte =:fileBinaryByte where c.id =:ID")
    public void updateFileBinaryByteByID(@Param("fileBinaryByte") short fileBinaryByte, @Param("ID") long id);
}
