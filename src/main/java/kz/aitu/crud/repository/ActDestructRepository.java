package kz.aitu.crud.repository;

import kz.aitu.crud.model.ActDestruct;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ActDestructRepository extends CrudRepository<ActDestruct, Long> {
    @Modifying
    @Transactional
    @Query("update ActDestruct c set c.base =:base where c.id =:ID")
    public void updateBaseByID(@Param("base") String base, @Param("ID") long id);
}
