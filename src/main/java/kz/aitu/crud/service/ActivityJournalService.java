package kz.aitu.crud.service;

import kz.aitu.crud.model.ActivityJournal;
import kz.aitu.crud.repository.ActivityJournalRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ActivityJournalService {
    private final ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    public ResponseEntity<?> getActivityJournals(){
        return ResponseEntity.ok(activityJournalRepository.findAll());
    }

    public ResponseEntity<?> findActivityJournalByID(long id){
        return ResponseEntity.ok(activityJournalRepository.findById(id));
    }

    public void deleteActivityJournalByID(long id){
        activityJournalRepository.deleteById(id);
    }

    public void updateActivityJournalByID(long id, String eventType){
        activityJournalRepository.updateEventTypeByID(eventType, id);
    }

    public ActivityJournal create(ActivityJournal activityJournal){
        return activityJournalRepository.save(activityJournal);
    }
}
