package kz.aitu.crud.service;

import kz.aitu.crud.repository.AuthRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final AuthRepository authRepository;

    public AuthService(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(authRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(authRepository.findById(id));
    }

    public void deleteByID(long id){
        authRepository.deleteById(id);
    }

    public void updateByID(long id, String username){
        authRepository.updateUsernameByID(username, id);
    }
}
