package kz.aitu.crud.service;

import kz.aitu.crud.repository.CaseIndexRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CaseIndexService {
    private final CaseIndexRepository caseIndexRepository;

    public CaseIndexService(CaseIndexRepository caseIndexRepository) {
        this.caseIndexRepository = caseIndexRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(caseIndexRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(caseIndexRepository.findById(id));
    }

    public void deleteByID(long id){
        caseIndexRepository.deleteById(id);
    }

    public void updateByID(long id, String titleEN){
        caseIndexRepository.updateTitleEnByID(titleEN, id);
    }
}
