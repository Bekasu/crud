package kz.aitu.crud.service;

import kz.aitu.crud.repository.ActDestructRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class ActDestructService {
    private final ActDestructRepository actDestructRepository;

    public ActDestructService(ActDestructRepository actDestructRepository) {
        this.actDestructRepository = actDestructRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(actDestructRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(actDestructRepository.findById(id));
    }

    public void deleteByID(long id){
        actDestructRepository.deleteById(id);
    }

    public void updateByID(long id, String base){
        actDestructRepository.updateBaseByID(base, id);
    }
}
