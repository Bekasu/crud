package kz.aitu.crud.controller;

import kz.aitu.crud.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyController {
    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }

    @GetMapping("/crud/searchKeys")
    public ResponseEntity<?> getSearchKeys() {
        return ResponseEntity.ok(searchKeyService.getAll());
    }

    @GetMapping("/crud/searchKey/{id}")
    public ResponseEntity<?> findSearchKeyByID(@PathVariable long id){
        return ResponseEntity.ok(searchKeyService.findByID(id));
    }

    @DeleteMapping("/crud/deleteSearchKey/{id}")
    public void deleteSearchKeyByID(@PathVariable long id){
        searchKeyService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateSearchKey/{id}/{name}", method = RequestMethod.GET)
    public void updateSearchKeyByID(@PathVariable("id") long id, @PathVariable("name") String name){
        searchKeyService.updateByID(id, name);
    }
}
