package kz.aitu.crud.controller;

import kz.aitu.crud.service.ActDestructService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActDestructController {
    private final ActDestructService actDestructService;

    public ActDestructController(ActDestructService actDestructService) {
        this.actDestructService = actDestructService;
    }

    @GetMapping("/crud/actDestructs")
    public ResponseEntity<?> getActDestructs() {
        return ResponseEntity.ok(actDestructService.getAll());
    }

    @GetMapping("/crud/actDestruct/{id}")
    public ResponseEntity<?> findActDestructByID(@PathVariable long id){
        return ResponseEntity.ok(actDestructService.findByID(id));
    }

    @DeleteMapping("/crud/deleteActDestruct/{id}")
    public void deleteActDestructByID(@PathVariable long id){
        actDestructService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateActDestruct/{id}/{base}", method = RequestMethod.GET)
    public void updateActDestructByID(@PathVariable("id") long id, @PathVariable("base") String base){
        actDestructService.updateByID(id, base);
    }
}

