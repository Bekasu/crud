package kz.aitu.crud.controller;

import kz.aitu.crud.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitController {
    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping("/crud/companyUnits")
    public ResponseEntity<?> getCompanyUnits() {
        return ResponseEntity.ok(companyUnitService.getAll());
    }

    @GetMapping("/crud/companyUnit/{id}")
    public ResponseEntity<?> findCompanyUnitByID(@PathVariable long id){
        return ResponseEntity.ok(companyUnitService.findByID(id));
    }

    @DeleteMapping("/crud/deleteCompanyUnit/{id}")
    public void deleteCompanyUnitByID(@PathVariable long id){
        companyUnitService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateCompanyUnit/{id}/{nameEN}", method = RequestMethod.GET)
    public void updateCompanyUnitByID(@PathVariable("id") long id, @PathVariable("nameEN") String nameEN){
        companyUnitService.updateByID(id, nameEN);
    }

}
