package kz.aitu.crud.controller;

import kz.aitu.crud.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/crud/authorizations")
    public ResponseEntity<?> getActivityJournals(){
        return ResponseEntity.ok(authService.getAll());
    }

    @GetMapping("/crud/authorization/{id}")
    public ResponseEntity<?> findAuthorizationByID(@PathVariable long id){
        return ResponseEntity.ok(authService.findByID(id));
    }

    @DeleteMapping("/crud/deleteAuthorization/{id}")
    public void deleteAuthorizationByID(@PathVariable long id){
        authService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateAuthorization/{id}/{username}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("username") String username){
        authService.updateByID(id, username);
    }
}
