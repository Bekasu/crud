package kz.aitu.crud.controller;

import kz.aitu.crud.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("/crud/fileRoutingServices")
    public ResponseEntity<?> getFileRoutingServices() {
        return ResponseEntity.ok(fileRoutingService.getAll());
    }

    @GetMapping("/crud/fileRoutingService/{id}")
    public ResponseEntity<?> findFileRoutingServiceByID(@PathVariable long id){
        return ResponseEntity.ok(fileRoutingService.findByID(id));
    }

    @DeleteMapping("/crud/deleteFileRoutingService/{id}")
    public void deleteFileRoutingServiceByID(@PathVariable long id){
        fileRoutingService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateFileRoutingService/{id}/{tableName}", method = RequestMethod.GET)
    public void updateFileRoutingServiceByID(@PathVariable("id") long id, @PathVariable("tableName") String tableName){
        fileRoutingService.updateByID(id, tableName);
    }
}
