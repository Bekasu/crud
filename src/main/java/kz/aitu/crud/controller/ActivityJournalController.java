package kz.aitu.crud.controller;

import kz.aitu.crud.model.ActivityJournal;
import kz.aitu.crud.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("/crud/activityJournals")
    public ResponseEntity<?> getActivityJournals(){
        return activityJournalService.getActivityJournals();
    }

    @GetMapping("/crud/activityJournal/{id}")
    public ResponseEntity<?> findActivityJournalByID(@PathVariable long id){
        return activityJournalService.findActivityJournalByID(id);
    }

    @DeleteMapping("/crud/deleteActivityJournal/{id}")
    public void deleteActivityJournalByID(@PathVariable long id){
        activityJournalService.deleteActivityJournalByID(id);
    }

    @RequestMapping(value = "/crud/updateActivityJournal/{id}/{eventType}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("eventType") String eventType){
        activityJournalService.updateActivityJournalByID(id,eventType);
    }

    @PostMapping("/crud/ActivityJournal")
    public ResponseEntity<?> createActivityJournal(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.create(activityJournal));
    }

}

