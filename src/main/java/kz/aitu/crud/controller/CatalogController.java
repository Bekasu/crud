package kz.aitu.crud.controller;

import kz.aitu.crud.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/crud/catalogs")
    public ResponseEntity<?> getCatalogs() {
        return ResponseEntity.ok(catalogService.getAll());
    }

    @GetMapping("/crud/catalog/{id}")
    public ResponseEntity<?> findCatalogByID(@PathVariable long id){
        return ResponseEntity.ok(catalogService.findByID(id));
    }

    @DeleteMapping("/crud/deleteCatalog/{id}")
    public void deleteCatalogByID(@PathVariable long id){
        catalogService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateCatalog/{id}/{nameEN}", method = RequestMethod.GET)
    public void updateCatalogByID(@PathVariable("id") long id, @PathVariable("nameEN") String nameEN){
        catalogService.updateByID(id, nameEN);
    }
}
