package kz.aitu.crud.controller;

import kz.aitu.crud.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/crud/users")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(usersService.getAll());
    }

    @GetMapping("/crud/user/{id}")
    public ResponseEntity<?> findUserByID(@PathVariable long id){
        return ResponseEntity.ok(usersService.findByID(id));
    }

    @DeleteMapping("/crud/deleteUser/{id}")
    public void deleteUserByID(@PathVariable long id){
        usersService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateUser/{id}/{name}", method = RequestMethod.GET)
    public void updateUserByID(@PathVariable("id") long id, @PathVariable("name") String name){
        usersService.updateByID(id, name);
    }
}
