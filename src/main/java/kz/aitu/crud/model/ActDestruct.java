package kz.aitu.crud.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "actdestruct")
public class ActDestruct {
    @Id
    private long id;
    private String actnumber;
    private String base;
    private long structuralsubdivisionid;
    private long createdtimestamp;
    private long createdby;
    private long updatedtimestamp;
    private long updatedby;
}
